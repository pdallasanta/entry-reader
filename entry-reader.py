#!/usr/bin/python
# -*- coding: utf-8 -*-

from datetime import datetime
import re
import os
import sys
import curses
import time

try:
    import csv
except ImportError, e:
    print "Package 'csv' nao instalada. Impressao do CSV (comando 'p') nao vai funcionar."

try:
    from pyfiglet import figlet_format
except ImportError, e:
    print "Package 'pyfiglet' nao instalada."
    def figlet_format(text, font="", width=0):
        return ""

# Constants
BOTTLES_PER_ENTRY = 3 
JUDGING_ID_LABELS = BOTTLES_PER_ENTRY + 2
CAP_ID_LABELS = BOTTLES_PER_ENTRY

CSV_FILENAME = os.path.expanduser("~/Desktop/entries.csv")
ENTRY_ID_LEN = 4
JUDGING_ID_LEN = 6
CAP_ID_LEN = 4
CASE_ID_LEN = 5

# Globals
entry_table = dict()

# Window properties
screen = None
HEIGHT = 0
WIDTH = 0

def entry_input():
    ''' Input new entry data '''
    
    skipped = 0

    # Read bottle labels
    for i in range(BOTTLES_PER_ENTRY):


        while 1:
            status_msg("Rotulo #{:d}/{:d}: ".format(i + 1, BOTTLES_PER_ENTRY), curses.COLOR_WHITE)
            code = screen.getstr()

            if code == "":
                continue

            if code.lower() == "r":
                status_msg("Reiniciando...", curses.COLOR_YELLOW)
                time.sleep(1)
                return entry_input()

            if code.lower() == "q":
                status_msg("Encerrando sessao", curses.COLOR_RED)
                exit(0)
            
            if code.lower() == "p":
                print_csv()
                continue

            if code.lower() == '?':
                print_man()
                continue

            if code.lower() == "s":
                if i > 0:
                    status_msg("Pulando garrafa #{:d}".format(i + 1), curses.COLOR_YELLOW)
                    skipped = skipped + 1
                    break
                else:
                    error_msg("Leia pelo menos uma amostra!")
                    continue

            if code.lower() == "d":
                if i > 0:
                    status_msg("Amostra {:s} desclassificada!".format(entry_id), curses.COLOR_RED)
                    return entry_id, "X", "X", i, -1
                else:
                    error_msg("Leia pelo menos uma amostra!")
                    continue

            if len(code) != ENTRY_ID_LEN:
                error_msg("ERRO! Codigo deve ter {:d} digitos".format(ENTRY_ID_LEN))
                continue

            if re.search(r'\D', code):
                error_msg("ERRO! Codigo invalido!")
                continue

            if code in entry_table:
                error_msg(("ERRO! Amostra {:s} ja foi registrada. Verifique engradado {:s}, " +\
                        "etiqueta {:s}.").format(code, \
                        entry_table[code]['case'], entry_table[code]['judging']))
                time.sleep(3)
                continue

            if i == 0:
                entry_id = code
            else:
                if entry_id != code:
                    error_msg("ERRO! Codigo diferente do anterior! Verifique e leia novamente!")
                    continue

            break

        beep()
        screen.clear()
        screen.addstr(1, 0, figlet_format("ROTULO {:d}/{:d}".format(i+1, BOTTLES_PER_ENTRY), font='banner3', width=WIDTH),\
                curses.color_pair(curses.COLOR_YELLOW))

        if len(code) == ENTRY_ID_LEN:
            screen.addstr(15, 0, figlet_format(code, font='banner3', width=WIDTH), curses.color_pair(curses.COLOR_GREEN))
        else:
            screen.addstr(15, 0, figlet_format("-"*ENTRY_ID_LEN, font='banner3', width=WIDTH), curses.color_pair(curses.COLOR_GREEN))

        screen.refresh()

    n_bottles = BOTTLES_PER_ENTRY - skipped

    # Get judging ID
    for i in range(JUDGING_ID_LABELS):
        while 1:
            status_msg("Etiqueta de julgamento #{:d}/{:d}: ".format(i + 1, JUDGING_ID_LABELS), curses.COLOR_WHITE)
            code = screen.getstr()

            if code == "":
                continue

            if code.lower() == "r":
                status_msg("Reiniciando...", curses.COLOR_YELLOW)
                time.sleep(1)
                return entry_input()

            if code.lower() == "q":
                status_msg("Encerrando sessao", curses.COLOR_RED)
                exit(0)

            if code.lower() == "p":
                print_csv()
                continue

            if code.lower() == '?':
                print_man()
                continue

            if code.lower() == "d":
                status_msg("Amostra {:s} desclassificada!".format(entry_id), curses.COLOR_RED)
                return entry_id, "X", "X", n_bottles, -1

            if len(code) != JUDGING_ID_LEN:
                error_msg("ERRO! Codigo deve ter {:d} digitos".format(JUDGING_ID_LEN))
                continue

            if re.search(r'[^0-9]', code):
                error_msg("ERRO! Codigo invalido!")
                continue

            if i == 0:
                judging_id = code
            else:
                if judging_id != code:
                    error_msg("ERRO! Codigo diferente do anterior! Verifique e leia novamente!")
                    continue

            screen.clear()
            screen.addstr(1, 0, figlet_format("ETIQUETA {:d}/{:d}".format(i+1, JUDGING_ID_LABELS), font='banner3', width=WIDTH),\
                    curses.color_pair(curses.COLOR_YELLOW))
            screen.addstr(15, 0, figlet_format(code, font='banner3', width=WIDTH), curses.color_pair(curses.COLOR_GREEN))
            screen.refresh()
            beep()
            break


    # Get cap judging ID
    n_caps = min(n_bottles, CAP_ID_LABELS)
    for i in range(n_caps):
        while 1:
            status_msg("Tampa #{:d}/{:d}: ".format(i + 1, n_caps), curses.COLOR_WHITE)
            code = screen.getstr()

            if code == "":
                continue

            if code.lower() == "r":
                status_msg("Reiniciando...", curses.COLOR_YELLOW)
                time.sleep(1)
                return entry_input()

            if code.lower() == "q":
                status_msg("Encerrando sessao", curses.COLOR_RED)
                exit(0)

            if code.lower() == "p":
                print_csv()
                continue

            if code.lower() == '?':
                print_man()
                continue

            if code.lower() == "d":
                status_msg("Amostra {:s} desclassificada!".format(entry_id), curses.COLOR_RED)
                return entry_id, "X", "X", n_bottles, -1

            if len(code) != CAP_ID_LEN:
                error_msg("ERRO! Codigo deve ter {:d} digitos".format(CAP_ID_LEN))
                continue

            if re.search(r'[^0-9]', code):
                error_msg("ERRO! Codigo invalido!")
                continue

            cap_id = code

            if int(cap_id) != int(judging_id):
                error_msg("ERRO! Codigo diferente da etiqueta de julgamento! Verifique e leia novamente!")
                continue
            
            screen.clear()
            screen.addstr(1, 0, figlet_format("TAMPA {:d}/{:d}".format(i+1, n_caps), font='banner3', width=WIDTH),\
                    curses.color_pair(curses.COLOR_YELLOW))
            screen.addstr(15, 0, figlet_format(code, font='banner3', width=WIDTH), curses.color_pair(curses.COLOR_GREEN))
            screen.refresh()
            beep()
            break

    # Get case id
    while 1:
        status_msg("Engradado: ", curses.COLOR_WHITE)
        code = screen.getstr()

        if code == "":
            continue

        if code.lower() == "r":
            status_msg("Reiniciando...", curses.COLOR_YELLOW)
            time.sleep(1)
            return entry_input()

        if code.lower() == "q":
            status_msg("Encerrando sessao", curses.COLOR_RED)
            exit(0)

        if code.lower() == "p":
            print_csv()
            continue

        if code.lower() == '?':
            print_man()
            continue

        if code.lower() == "d":
            status_msg("Amostra {:s} desclassificada!".format(entry_id), curses.COLOR_RED)
            return entry_id, "X", "X", n_bottles, -1

        if len(code) != CASE_ID_LEN:
            error_msg("ERRO! Codigo deve ter {:d} digitos".format(CASE_ID_LEN))
            continue

        if re.search(r'[^0-9]', code):
            error_msg("ERRO! Codigo invalido!")
            continue

        case_id = code

        screen.clear()
        screen.addstr(1, 0, figlet_format("ENGRADADO", font='banner3', width=WIDTH), curses.color_pair(curses.COLOR_YELLOW))
        screen.addstr(15, 0, figlet_format(code, font='banner3', width=WIDTH), curses.color_pair(curses.COLOR_GREEN))
        screen.refresh()
        beep()
        break

    return entry_id, judging_id, case_id, n_bottles, 0

def beep():
    ''' Play a beep '''
    os.system("aplay scanner.ogg 2>/dev/null &")

def status_msg(s, color):
    ''' Set status message '''
    global screen
    screen.addstr(HEIGHT - 2, 0, " "*WIDTH)
    screen.addstr(HEIGHT - 2, 0, s, curses.color_pair(color))
    screen.refresh()

def error_msg(s):
    ''' Error message '''
    status_msg(s, curses.COLOR_RED)
    os.system("aplay error.ogg 2>/dev/null &")
    time.sleep(1)

def print_man():
    ''' Print manual info '''
    screen.addstr(HEIGHT - man.count('\n') - 3, 0, man, curses.COLOR_WHITE)
    screen.refresh()

def print_csv():
    ''' Print CSV file '''
    f = open(CSV_FILENAME, "r")

    format_str = "\r| {:" + str(max(ENTRY_ID_LEN, len('Entry'))) + 's} | {:' + str(max(JUDGING_ID_LEN, len('Judging'))) + 's} | ' +\
            '{:' + str(max(CASE_ID_LEN, len('Case'))) + 's} | {:' + str(len('N Bottles')) + 's} | {:' + str(len('10/05/2015 21:53')) + 's} |'

    screen.clear()
    screen.refresh()
    curses.setsyx(0,0)

    with open(CSV_FILENAME, 'rb') as csvfile:
        reader = csv.reader(csvfile, delimiter=';')
        for row in reader:
            if len(row) < 5:
                error_msg("Erro de formato no arquivo CSV!")
                print('\r' + ";".join(row))
                return False
            print(format_str.format(row[0], row[1], row[2], row[3], row[4]))
    print('\r')
    f.close()


# Welcome message
logo = '''
                        ##### #   ## # # #   ###  # ###   #### ## #   # #  ## #   #### ## #   ## # # #
                      # ##### #   ## # # #   ###  # ###   #### ## #   # #  ## #   #### ## #   ## # # ##
                     ## ##### #   ## # # #   ###  # ###   #### ## #   # #  ## #   #### ## #   ## # # ###
                  ## ## ##### #   ## # # #   ###  # ###   #### ## #   # #  ## #   #### ## #   ## # # ###
 # ##  # # ### #  ## ## ##### #   ## # # #   ###  # ###   #### ## #   # #  ## #   #### ## #   ## # # ###
 # ##  # # ### #  ## ## ##### #   ## # # #   ###  # ###   #### ## #   # #  ## #   #### ## #   ## # # ###
 # ##  # # ### #  ## ## ##### #   ## # # #   ###  # ###   #### ## #   # #  ## #   #### ## #   ## # # ###
 # ##  # # ### #  ## ## ##### #   ## # # #   ###  # ###   #### ## #   # #  ## #   #### ## #   ## # # ###
                  ## ## ##### #   ## # # #   ###  # ###   #### ## #   # #  ## #   #### ## #   ## # # ###
                     ## ##### #   ## # # #   ###  # ###   #### ## #   # #  ## #   #### ## #   ## # # ###
                      # ##### #   ## # # #   ###  # ###   #### ## #   # #  ## #   #### ## #   ## # # ##
                        ##### #   ## # # #   ###  # ###   #### ## #   # #  ## #   #### ## #   ## # # #

                                   B R E W   C O M P E T I T I O N   E N T R Y   R E A D E R
'''

about = '''(c) 2015 - Acerva Gaucha
Autor: Paulo Dalla Santa <pdallasanta@yahoo.com.br>
Versao: 0.1
'''

man = '''Comandos:
   Q - sair
   R - reiniciar a leitura da amostra atual
   P - imprimir CSV
   S - pular etiquetas de garrafas faltantes ou danificadas (skip)
   D - desclassificar amostra
   ? - exibe lista de comandos
'''


def main(stdscr):
    ''' Main loop'''

    global screen
    global HEIGHT
    global WIDTH

    # Curses init
    screen = stdscr
    HEIGHT, WIDTH = screen.getmaxyx()
    curses.echo()
    curses.start_color()
    curses.use_default_colors()
    curses.init_pair(curses.COLOR_BLUE, curses.COLOR_BLUE, -1)
    curses.init_pair(curses.COLOR_WHITE, curses.COLOR_WHITE, -1)
    curses.init_pair(curses.COLOR_GREEN, curses.COLOR_GREEN, -1)
    curses.init_pair(curses.COLOR_YELLOW, curses.COLOR_YELLOW, -1)
    curses.init_pair(curses.COLOR_RED, curses.COLOR_RED, -1)

    # Print welcome screen
    screen.addstr(1, 0, logo + '\n', curses.color_pair(curses.COLOR_BLUE))
    screen.addstr(about + '\n', curses.color_pair(curses.COLOR_WHITE))
    screen.addstr(man + '\n', curses.color_pair(curses.COLOR_YELLOW))
    screen.refresh()

    # Check if CSV file exists
    if os.path.isfile(CSV_FILENAME):
        screen.addstr("Arquivo {} ja existe. Amostras serao incluidas no final do arquivo original.\n".format(CSV_FILENAME))
    else:
        screen.addstr("Arquivo {} nao existe. Criando...\n".format(CSV_FILENAME))
        f=open(CSV_FILENAME, 'w')
        f.write("Entry;Judging;Case;N Bottles;Timestamp\n")
        f.close()

    screen.refresh()

    with open(CSV_FILENAME, 'rb') as csvfile:
        reader = csv.reader(csvfile, delimiter=';')
        for row in reader:
            if len(row) < 5:
                screen.addstr("Linha invalida no arquivo CSV:\n{:s}\n".format(";".join(row)))
                screen.refresh()
                continue

            csv_entry = row[0]
            csv_judging = row[1]
            csv_case = row[2]
            csv_nbottles = row[3]
            csv_timestamp = row[4]

            if csv_judging == "X":
                continue

            if csv_entry in entry_table:
                print("\rAmostra {} duplicada no arquivo CSV.".format(csv_entry))
                continue

            entry_table[csv_entry] = {'judging':csv_judging, 'case':csv_case, \
                    'n_bottles':csv_nbottles, 'timestamp':csv_timestamp}

    print("\r\n")
    screen.refresh()

    # Entry input loop
    while 1:
        entry_id, judging_id, case_id, n_bottles, status = entry_input()
        timestamp = datetime.now().strftime('%d/%m/%Y %H:%M')

        csv_string = "{:s};{:s};{:s};{:d};{:s}\n".format(entry_id, judging_id, case_id, n_bottles, timestamp)

        f=open(CSV_FILENAME, 'a')
        f.write(csv_string)
        f.close()

        if status == 0:
            entry_table[entry_id] = {'judging':judging_id, 'case':case_id, 'n_bottles':n_bottles, \
                    'timestamp':timestamp}

            status_msg("Amostra {:s} cadastrada com sucesso (judging number = {:s}, engradado = {:s})\n".format( \
                entry_id, judging_id, case_id), curses.COLOR_GREEN)
        else:
            status_msg("Amostra {:s} desclassificada!\n".format(entry_id), curses.COLOR_RED)
        os.system("aplay done.ogg 2>/dev/null &")
        time.sleep(2)
        screen.clear()

        if status == 0:
            screen.addstr(15, 0, figlet_format("{} = {}".format(entry_id, judging_id), font='banner3', width=WIDTH),\
                    curses.color_pair(curses.COLOR_GREEN))
        else:
            screen.addstr(15, 0, figlet_format("{} = {}".format(entry_id, judging_id), font='banner3', width=WIDTH),\
                    curses.color_pair(curses.COLOR_RED))
        screen.refresh()


# Parse command line arguments
if len(sys.argv) > 2:
    print "ERRO: numero de argumentos invalido. Sitaxe: entry-reader.py [csv filename]"
    exit(1)
elif len(sys.argv) == 2:
    CSV_FILENAME = sys.argv[1]

# Call main
curses.wrapper(main)

# Descrição #

Leitor de código de barras offline para triagem de garrafas.  

Possibilita triagem rápida, sem necessidade de usar mouse e teclado durante a operação. A realimentação do usuário é através de beeps e outras indicações sonoras, dispensando a conferência visual de dados na tela.  

Todos os dados são verificados (por exemplo, se os códigos de todas as garrafas pertencentes a determinada amostra são todos iguais e se o número de dígitos do código confere com o esperado).  

O script gera um arquivo CSV que deverá ser importado para o banco de dados do BCOE&M (Consultar Balbinot para mais informações - e possivelmente script para fazer isso). O CSV possui as seguintes colunas:  

Entry_id; Judging_id; Case_id; N_bottles; Timestamp  

Onde:  

Entry_id = número da inscrição  
Judging_id = número de julgamento  
Case_id = número do engradado  
N_bottles = número de garrafas recebidas  
Timestamp = data/hora do processamento  

# Sintaxe #

entry_reader.py [csv filename]  

OBS: se o arquivo CSV já existe, os novos dados serão incluídos ao final dos existentes.

# Parâmetros configuráveis (no fonte) #

BOTTLES_PER_ENTRY = 3  
JUDGING_ID_LABELS = BOTTLES_PER_ENTRY*2 + 5  
CAP_ID_LABELS = BOTTLES_PER_ENTRY  
  
ENTRY_ID_LEN = 4  
JUDGING_ID_LEN = 6  
CAP_ID_LEN = 4  
CASE_ID_LEN = 5  
  
CSV_FILENAME = os.path.expanduser("~/Desktop/entries.csv")